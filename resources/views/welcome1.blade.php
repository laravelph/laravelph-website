<!DOCTYPE html>
<html lang="en">
    <head>
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-2B7FM57XC9"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            
            gtag('config', 'G-2B7FM57XC9');
        </script>
	<meta charset="utf-8">
	<meta name="description" content="LaravelPH is a community of Laravel Developers (Artisans) from the Philipines ">
	<meta name="author" content="Joe">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>LaravelPH: Home </title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <!-- Styles -->
	<link href="css/pure.css" rel="stylesheet" type="text/css" />
	<style>
svg.icon { width: 32px; height: 32px; fill: rgb(63,103,108); background: transparent; color:#4267B2 }
svg.alt { width: 128px; height: 128px; fill: rgb(30,30,30); }
	</style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/alpinejs/2.3.0/alpine-ie11.min.js" integrity="sha512-Atu8sttM7mNNMon28+GHxLdz4Xo2APm1WVHwiLW9gW4bmHpHc/E2IbXrj98SmefTmbqbUTOztKl5PDPiu0LD/A==" crossorigin="anonymous"></script>

    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand" href="#page-top">
                <img src="https://raw.githubusercontent.com/laravelph/art/master/icon.svg" alt="LaravelPH Logo Icon " style="  filter: invert(40%) sepia(23%) saturate(4478%) hue-rotate(338deg) brightness(101%) contrast(97%); " height="150" width="150" />
		</a>
		 <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
		 <li class="disabled nav-item">
			<span style="position: relative; margin-top: 12px; display: inline-block;color: #c00;">Join us on </span>
		</li>
     <li class="nav-item active" >
			<a class="nav-link" href="#">
			<svg xmlns="http://www.w3.org/2000/svg" fill="#fff" viewBox="0 0 24 24" class="icon icon-style"><title>facebook</title><path d="M24 12.073c0-6.627-5.373-12-12-12s-12 5.373-12 12c0 5.99 4.388 10.954 10.125 11.854v-8.385H7.078v-3.47h3.047V9.43c0-3.007 1.792-4.669 4.533-4.669 1.312 0 2.686.235 2.686.235v2.953H15.83c-1.491 0-1.956.925-1.956 1.874v2.25h3.328l-.532 3.47h-2.796v8.385C19.612 23.027 24 18.062 24 12.073z"></path></svg>
		</a>
</span>
      </li>
      <li class="nav-item">
	<a class="nav-link" href="https://www.meetup.com/laravel-philippines/">
	  <img src="img/meetup.png" style="color: #e51937;" width="32" height="32">
	</a>
      </li>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars ms-1"></i>
                </button>
            </div>
        </nav>
        <!-- Masthead-->
        <header class="masthead">
            <div class="container">
                <div class="masthead-subheading">Welcome To LaravelPH!</div>
                <div class="masthead-heading"></div>
                <a class="btn btn-primary btn-xl text-uppercase" href="#connect">Join us</a>
            </div>
        </header>
        <div class="page-section" id="connect">
            <div class="container">
                <div class="flex-row d-flex justify-content-between">
                    <div class="col col-sm-3" style="margin-left: 15%;"a>
                        <a href="https://www.facebook.com/groups/185448994945295" title="facebook">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="#fff" viewBox="0 0 24 24" class="icon-large icon-style"><title>facebook</title><path d="M24 12.073c0-6.627-5.373-12-12-12s-12 5.373-12 12c0 5.99 4.388 10.954 10.125 11.854v-8.385H7.078v-3.47h3.047V9.43c0-3.007 1.792-4.669 4.533-4.669 1.312 0 2.686.235 2.686.235v2.953H15.83c-1.491 0-1.956.925-1.956 1.874v2.25h3.328l-.532 3.47h-2.796v8.385C19.612 23.027 24 18.062 24 12.073z"></path></svg>
                        </a>
                    </div>
                    <div class="col col-sm-3">
			<a href="https://discord.gg/dTWFd3z" title="discord">
                   </div>
                    <div class="col col-sm-3">
			<a href="mailto:laravelph@gmail.com" title="Email us">
<img src="img/emailicon.png" width="128" height="100">
			</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="col-xs-12 lex justify-center pt-8 sm:justify-start sm:pt-0">
                <p>LaravelPH is a community of laravel enthusiasts! We have online meetups and trainings!</p>
            </div>
            <div class="footer" style="position:absolute; bottom: 2rem">
                <div class="ml-4 text-center text-sm text-gray-500 sm:text-right sm:ml-0">LaravelPH &copy 2016- 2022 </div>
            </div>
        </div>
        </div>
    </body>
</html>
